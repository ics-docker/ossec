# Description

OSSEC v3.3.0

OSSEC is a full platform to monitor and control your systems. It mixes together all the aspects of HIDS (host-based intrusion detection), log monitoring and SIM/SIEM together in a simple, powerful and open source solution. [OSSEC website](https://www.ossec.net/)

The container is based on Centos 7. [Official OSSEC container](https://github.com/ossec/ossec-hids)

By default this container will create a volume to store configuration, log and agent key data 
under /var/ossec/data.  Additionally it is configured with a local instance of postfix to 
send alert notifications.


  
## Launch the container:

`docker run -d -p 1514:1514/udp -p 1515:1515/tcp --name ossec-server <image>`


## Launch the container, specifying a volume:


`docker volume create ossec-data
docker run -d -p 1514:1514/udp -p 1515:1515/tcp -v ossec-data:/var/ossec/data --name ossec-server atomicorp/ossec-docker`


## Stop the container:

`docker stop ossec-server`

## Re-start the container:

`docker start ossec-server`


## Attach to running container:

`docker exec -it ossec-server  bash`
